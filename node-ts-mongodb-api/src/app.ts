// src/app.ts
import express from 'express';
import tasksRouter from './routes/tasks';
import { client } from './data-source';

const app = express();
const port = process.env.PORT || 3000;

async function connectDB() {
  try {
    await client.connect();
    console.log('Connected to MongoDB');
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    process.exit(1);
  }
}

app.use(express.json());

app.use('/api/v1', tasksRouter);

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
  connectDB();
});
