// src/routes/tasks.ts
import express, { Router, Request, Response } from 'express';
import { Collection, ObjectId } from 'mongodb';
import { client, dbName } from '../data-source';

const router = Router();

router.use(express.json());

router.get('/tasks', async (req: Request, res: Response) => {
  const db = client.db(dbName);
  const tasksCollection: Collection = db.collection('tasks');

  try {
    const tasks = await tasksCollection.find({}).toArray();
    res.status(200).json(tasks);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve tasks.' });
  }
});

router.get('/tasks/:id', async (req: Request, res: Response) => {
  const db = client.db(dbName);
  const tasksCollection: Collection = db.collection('tasks');

  try {
    const task = await tasksCollection.findOne({ _id: new ObjectId(req.params.id) });
    if (!task) {
      res.status(404).json({ error: 'Task not found.' });
    } else {
      res.status(200).json(task);
    }
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve task.' });
  }
});

// Create a new task
router.post('/tasks', async (req: Request, res: Response) => {
  const db = client.db(dbName);
  const tasksCollection: Collection = db.collection('tasks');

  const { title, description } = req.body;

  if (!title || !description) {
    return res.status(400).json({ error: 'Title and description are required.' });
  }

  const newTask = {
    title,
    description,
    done: false,
  };

  try {
    const result = await tasksCollection.insertOne(newTask);
    res.status(201).json(result);
  } catch (error) {
    res.status(500).json({ error: 'Failed to create task.' });
  }
});

// Update an existing task
router.put('/tasks/:id', async (req: Request, res: Response) => {
  const db = client.db(dbName);
  const tasksCollection: Collection = db.collection('tasks');

  const { title, description, done } = req.body;

  if (!title || !description) {
    return res.status(400).json({ error: 'Title and description are required.' });
  }

  const taskID = new ObjectId(req.params.id);

  try {
    const result = await tasksCollection.findOneAndUpdate(
      { _id: taskID },
      { $set: { title, description, done } }
    );

    console.log(result);

    if (!result) {
      return res.status(404).json({ error: 'Task not found.' });
    }

    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: 'Failed to update task.' });
  }
});

// Delete a task
router.delete('/tasks/:id', async (req: Request, res: Response) => {
  const db = client.db(dbName);
  const tasksCollection: Collection = db.collection('tasks');

  const taskID = new ObjectId(req.params.id);

  try {
    const result = await tasksCollection.findOneAndDelete({ _id: taskID });

    console.log(result);

    if (!result) {
      return res.status(404).json({ error: 'Task not found.' });
    }

    res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ error: 'Failed to delete task.' });
  }
});

export default router;
